package View;

import Control.ControleOcorrencia;
import Control.ControlePrevencao;
import Model.Endereco;
import Model.Ocorrencia;
import Model.Prevencao;
import java.awt.Menu;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

public class TelaBombeiro extends javax.swing.JFrame {

    ControleOcorrencia umControleOcorrencia = new ControleOcorrencia();
    ControlePrevencao umControlePrevencao = new ControlePrevencao();
    private Ocorrencia umaOcorrencia;
    private Prevencao umaPrevencao;
    private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    
    public TelaBombeiro() {
        initComponents();
        this.jTableOcorrencias.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
    
    TelaBombeiro(Menu aThis, boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void limparCampos(){ 
        jTextFieldCidade.setText("");
        jTextFieldCidadeP.setText("");
        jTextFieldData.setText(null);
        jTextFieldDataP.setText(null);
        jTextFieldEstado.setText("");
        jTextFieldEstadoP.setText("");
        jTextFieldGrupo.setText("");
        jTextFieldHorario.setText("");
        jTextFieldHorarioP.setText("");
        jTextFieldLogradouro.setText("");
        jTextFieldLogradouroP.setText("");
        jTextFieldNatureza.setText("");
        jTextFieldSubgrupo.setText("");
    }
    
    private void exibirInformacao(String info) {
        JOptionPane.showMessageDialog(this, info, "Atenção", JOptionPane.INFORMATION_MESSAGE);
    }
    
    private boolean validarCampos() {
        if (jTextFieldCidade.getText().trim().length() == 0) {
            this.exibirInformacao("O valor do campo 'Cidade' não foi informado.");
            jTextFieldCidade.requestFocus();
            return false;
        }
        
        if (jTextFieldEstado.getText().trim().length() == 0) {
            this.exibirInformacao("O valor do campo 'Estado' não foi informado.");
            jTextFieldEstado.requestFocus();
            return false;
        }
        
        if (jTextFieldGrupo.getText().trim().length() == 0) {
            this.exibirInformacao("O valor do campo 'Grupo' não foi informado.");
            jTextFieldGrupo.requestFocus();
            return false;
        }
        
        if (jTextFieldSubgrupo.getText().trim().length() == 0) {
            this.exibirInformacao("O valor do campo 'Subgrupo' não foi informado.");
            jTextFieldSubgrupo.requestFocus();
            return false;
        }
        
        if (jTextFieldHorario.getText().trim().length() == 0) {
            this.exibirInformacao("O valor do campo 'Horário' não foi informado.");
            jTextFieldHorario.requestFocus();
            return false;
        }
        
        if (jTextFieldData.getText().length() != 0) {
            try {
                dateFormat.parse(jTextFieldData.getText());
            } catch (ParseException ex) {
                this.exibirInformacao("O valor do campo 'Data' é inválido.");
                jTextFieldData.requestFocus();
                return false;
            }
        }
        
        if (jTextFieldLogradouro.getText().trim().length() == 0) {
            this.exibirInformacao("O valor do campo 'Logradouro' não foi informado.");
            jTextFieldLogradouro.requestFocus();
            return false;
        }
        
        if (jTextFieldNatureza.getText().trim().length() == 0) {
            this.exibirInformacao("O valor do campo 'Natureza' não foi informado.");
            jTextFieldNatureza.requestFocus();
            return false;
        }
        return true;
    }
        
    private boolean validarCamposP() {        
        if (jTextFieldCidadeP.getText().trim().length() == 0) {
            this.exibirInformacao("O valor do campo 'Cidade' não foi informado.");
            jTextFieldCidadeP.requestFocus();
            return false;
        }
        
        if (jTextFieldHorarioP.getText().trim().length() == 0) {
            this.exibirInformacao("O valor do campo 'Horário' não foi informado.");
            jTextFieldHorarioP.requestFocus();
            return false;
        }
        
        if (jTextFieldEstadoP.getText().trim().length() == 0) {
            this.exibirInformacao("O valor do campo 'Estado' não foi informado.");
            jTextFieldEstadoP.requestFocus();
            return false;
        }
        
        if (jTextFieldDataP.getText().length() != 0) {
            try {
                dateFormat.parse(jTextFieldDataP.getText());
            } catch (ParseException ex) {
                this.exibirInformacao("O valor do campo 'Data' é inválido.");
                jTextFieldDataP.requestFocus();
                return false;
            }
        }    

        if (jTextFieldLogradouroP.getText().trim().length() == 0) {
            this.exibirInformacao("O valor do campo 'Logradouro' não foi informado.");
            jTextFieldLogradouroP.requestFocus();
            return false;
        }     
        return true;
    }
    
    private void carregarListaOcorrencias() {
        ArrayList<Ocorrencia> listaOcorrencias = umControleOcorrencia.getListaOcorrencias();
        DefaultTableModel model = (DefaultTableModel) jTableOcorrencias.getModel();
        model.setRowCount(0);
        for (Ocorrencia o: listaOcorrencias) {
            model.addRow(new Date[]{o.getData()});
            model.addRow(new String[]{ o.getHorario(), o.getGrupo(), o.getSubgrupo(), o.getNatureza(),o.endereco.getLogradouro(), o.endereco.getCidade(), o.endereco.getEstado() });
        }
        jTableOcorrencias.setModel(model);
    }
    
    private void carregarListaPrevencao() {
        ArrayList<Prevencao> listaPrevencao = umControlePrevencao.getListaPrevencao();
        DefaultTableModel model = (DefaultTableModel) jTablePrevencao.getModel();
        model.setRowCount(0);
        for (Prevencao p: listaPrevencao) {
            model.addRow(new Date[]{p.getDataP()});
            model.addRow(new String[]{ p.getHorarioP(), p.endereco.getLogradouro(), p.endereco.getCidade(), p.endereco.getEstado() });
        }
        jTablePrevencao.setModel(model);
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanelCadastro = new javax.swing.JPanel();
        jLabelGrupo = new javax.swing.JLabel();
        jLabelSubgrupo = new javax.swing.JLabel();
        jTextFieldSubgrupo = new javax.swing.JTextField();
        jTextFieldGrupo = new javax.swing.JTextField();
        jLabelData = new javax.swing.JLabel();
        jTextFieldData = new javax.swing.JTextField();
        jLabelHorario = new javax.swing.JLabel();
        jTextFieldHorario = new javax.swing.JTextField();
        jLabelNatureza = new javax.swing.JLabel();
        jTextFieldNatureza = new javax.swing.JTextField();
        jLabelLogradouro = new javax.swing.JLabel();
        jTextFieldLogradouro = new javax.swing.JTextField();
        jLabelCidade = new javax.swing.JLabel();
        jTextFieldCidade = new javax.swing.JTextField();
        jLabelEstado = new javax.swing.JLabel();
        jTextFieldEstado = new javax.swing.JTextField();
        jButtonSalvar = new javax.swing.JButton();
        jPanelOcorrências = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableOcorrencias = new javax.swing.JTable();
        jButtonRemover = new javax.swing.JButton();
        jPanelPrevenção = new javax.swing.JPanel();
        jLabelDataP = new javax.swing.JLabel();
        jLabelHorarioP = new javax.swing.JLabel();
        jTextFieldDataP = new javax.swing.JTextField();
        jTextFieldHorarioP = new javax.swing.JTextField();
        jLabelLogradouroP = new javax.swing.JLabel();
        jLabelCidadeP = new javax.swing.JLabel();
        jLabelEstadoP = new javax.swing.JLabel();
        jTextFieldLogradouroP = new javax.swing.JTextField();
        jTextFieldCidadeP = new javax.swing.JTextField();
        jTextFieldEstadoP = new javax.swing.JTextField();
        jButtonSalvarP = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTablePrevencao = new javax.swing.JTable();
        jButtonRemoverP = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabelGrupo.setText("Grupo:");

        jLabelSubgrupo.setText("Subgrupo:");

        jTextFieldGrupo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldGrupoActionPerformed(evt);
            }
        });

        jLabelData.setText("Data:");

        jTextFieldData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldDataActionPerformed(evt);
            }
        });

        jLabelHorario.setText("Horário:");

        jLabelNatureza.setText("Natureza:");

        jTextFieldNatureza.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldNaturezaActionPerformed(evt);
            }
        });

        jLabelLogradouro.setText("Logradouro:");

        jLabelCidade.setText("Cidade:");

        jLabelEstado.setText("Estado:");

        jButtonSalvar.setText("Salvar");
        jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelCadastroLayout = new javax.swing.GroupLayout(jPanelCadastro);
        jPanelCadastro.setLayout(jPanelCadastroLayout);
        jPanelCadastroLayout.setHorizontalGroup(
            jPanelCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCadastroLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelNatureza)
                    .addComponent(jLabelHorario)
                    .addComponent(jLabelLogradouro)
                    .addComponent(jLabelCidade)
                    .addComponent(jButtonSalvar)
                    .addGroup(jPanelCadastroLayout.createSequentialGroup()
                        .addGroup(jPanelCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelSubgrupo)
                            .addComponent(jLabelGrupo)
                            .addComponent(jLabelData)
                            .addComponent(jLabelEstado))
                        .addGap(36, 36, 36)
                        .addGroup(jPanelCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldSubgrupo, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldGrupo, javax.swing.GroupLayout.PREFERRED_SIZE, 252, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldNatureza, javax.swing.GroupLayout.PREFERRED_SIZE, 349, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldData, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldHorario, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldLogradouro, javax.swing.GroupLayout.PREFERRED_SIZE, 426, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldCidade, javax.swing.GroupLayout.PREFERRED_SIZE, 364, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(234, Short.MAX_VALUE))
        );
        jPanelCadastroLayout.setVerticalGroup(
            jPanelCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCadastroLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelGrupo)
                    .addComponent(jTextFieldGrupo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelSubgrupo)
                    .addComponent(jTextFieldSubgrupo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNatureza)
                    .addComponent(jTextFieldNatureza, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelData)
                    .addComponent(jTextFieldData, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelHorario)
                    .addComponent(jTextFieldHorario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addGroup(jPanelCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelLogradouro)
                    .addComponent(jTextFieldLogradouro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelCidade)
                    .addComponent(jTextFieldCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelCadastroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelEstado)
                    .addComponent(jTextFieldEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 123, Short.MAX_VALUE)
                .addComponent(jButtonSalvar)
                .addGap(20, 20, 20))
        );

        jTabbedPane1.addTab("Cadastro", jPanelCadastro);

        jTableOcorrencias.setModel(new javax.swing.table.DefaultTableModel 
            (
                null,
                new String [] {
                    "Data", "Horario", "Grupo", "Subgrupo", "Natureza", "Logradouro", "Cidade", "Estado"
                }
            )
            {
                @Override    
                public boolean isCellEditable(int rowIndex, int mColIndex) {
                    return false;
                }
            });
            jTableOcorrencias.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jTableOcorrenciasMouseClicked(evt);
                }
            });
            jScrollPane1.setViewportView(jTableOcorrencias);

            jButtonRemover.setText("Remover");
            jButtonRemover.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButtonRemoverActionPerformed(evt);
                }
            });

            javax.swing.GroupLayout jPanelOcorrênciasLayout = new javax.swing.GroupLayout(jPanelOcorrências);
            jPanelOcorrências.setLayout(jPanelOcorrênciasLayout);
            jPanelOcorrênciasLayout.setHorizontalGroup(
                jPanelOcorrênciasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanelOcorrênciasLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanelOcorrênciasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanelOcorrênciasLayout.createSequentialGroup()
                            .addComponent(jButtonRemover)
                            .addGap(0, 0, Short.MAX_VALUE))
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 756, Short.MAX_VALUE))
                    .addContainerGap())
            );
            jPanelOcorrênciasLayout.setVerticalGroup(
                jPanelOcorrênciasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanelOcorrênciasLayout.createSequentialGroup()
                    .addGap(22, 22, 22)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 407, Short.MAX_VALUE)
                    .addGap(18, 18, 18)
                    .addComponent(jButtonRemover)
                    .addContainerGap())
            );

            jTabbedPane1.addTab("Ocorrências", jPanelOcorrências);

            jLabelDataP.setText("Data:");

            jLabelHorarioP.setText("Horário:");

            jLabelLogradouroP.setText("Logradouro:");

            jLabelCidadeP.setText("Cidade:");

            jLabelEstadoP.setText("Estado:");

            jButtonSalvarP.setText("Salvar");
            jButtonSalvarP.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButtonSalvarPActionPerformed(evt);
                }
            });

            jTablePrevencao.setModel(new javax.swing.table.DefaultTableModel 
                (
                    null,
                    new String [] {
                        "Data", "Horário", "Logradouro", "Estado", "Cidade"
                    }
                )
                {
                    @Override    
                    public boolean isCellEditable(int rowIndex, int mColIndex) {
                        return false;
                    }
                });
                jTablePrevencao.addMouseListener(new java.awt.event.MouseAdapter() {
                    public void mouseClicked(java.awt.event.MouseEvent evt) {
                        jTablePrevencaoMouseClicked(evt);
                    }
                });
                jScrollPane2.setViewportView(jTablePrevencao);

                jButtonRemoverP.setText("Remover");
                jButtonRemoverP.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        jButtonRemoverPActionPerformed(evt);
                    }
                });

                javax.swing.GroupLayout jPanelPrevençãoLayout = new javax.swing.GroupLayout(jPanelPrevenção);
                jPanelPrevenção.setLayout(jPanelPrevençãoLayout);
                jPanelPrevençãoLayout.setHorizontalGroup(
                    jPanelPrevençãoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelPrevençãoLayout.createSequentialGroup()
                        .addGroup(jPanelPrevençãoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelPrevençãoLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanelPrevençãoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanelPrevençãoLayout.createSequentialGroup()
                                        .addGroup(jPanelPrevençãoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabelDataP)
                                            .addComponent(jLabelHorarioP))
                                        .addGap(49, 49, 49)
                                        .addGroup(jPanelPrevençãoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jTextFieldHorarioP, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jTextFieldDataP, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(jPanelPrevençãoLayout.createSequentialGroup()
                                        .addGroup(jPanelPrevençãoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabelLogradouroP)
                                            .addComponent(jLabelCidadeP)
                                            .addComponent(jLabelEstadoP))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanelPrevençãoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jTextFieldLogradouroP, javax.swing.GroupLayout.PREFERRED_SIZE, 364, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jTextFieldCidadeP, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jTextFieldEstadoP, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(jPanelPrevençãoLayout.createSequentialGroup()
                                        .addComponent(jButtonSalvarP)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jButtonRemoverP))))
                            .addGroup(jPanelPrevençãoLayout.createSequentialGroup()
                                .addGap(40, 40, 40)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 535, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(205, Short.MAX_VALUE))
                );
                jPanelPrevençãoLayout.setVerticalGroup(
                    jPanelPrevençãoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelPrevençãoLayout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addGroup(jPanelPrevençãoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelDataP)
                            .addComponent(jTextFieldDataP, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelPrevençãoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelHorarioP)
                            .addComponent(jTextFieldHorarioP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanelPrevençãoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelLogradouroP)
                            .addComponent(jTextFieldLogradouroP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelPrevençãoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelCidadeP)
                            .addComponent(jTextFieldCidadeP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelPrevençãoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelEstadoP)
                            .addComponent(jTextFieldEstadoP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 101, Short.MAX_VALUE)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(27, 27, 27)
                        .addGroup(jPanelPrevençãoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButtonSalvarP)
                            .addComponent(jButtonRemoverP))
                        .addContainerGap())
                );

                jTabbedPane1.addTab("Prevenção", jPanelPrevenção);

                javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
                getContentPane().setLayout(layout);
                layout.setHorizontalGroup(
                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane1)
                );
                layout.setVerticalGroup(
                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane1)
                );

                pack();
            }// </editor-fold>//GEN-END:initComponents

    private void jTextFieldGrupoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldGrupoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldGrupoActionPerformed

    private void jTextFieldDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldDataActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldDataActionPerformed

    private void jButtonRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRemoverActionPerformed
        this.umControleOcorrencia.remover(umaOcorrencia);        
        this.carregarListaOcorrencias();
    }//GEN-LAST:event_jButtonRemoverActionPerformed

    private void jButtonSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarActionPerformed
        if (this.validarCampos() == false) {
            return;
        }
        
        String umGrupo = this.jTextFieldGrupo.getText();
        umaOcorrencia = new Ocorrencia(jTextFieldGrupo.getText());
        Endereco endereco = new Endereco();
        Date data;
        
        if (jTextFieldData.getText().length() == 0) {
            data = null;
        } else {
            try {
                data = dateFormat.parse(jTextFieldData.getText());
            } catch (ParseException ex) {
                exibirInformacao("Falha ao gravar a data de nascimento: " + ex.toString());
                return;
            }
        }
        
        umaOcorrencia.setSubgrupo(jTextFieldSubgrupo.getText());
        umaOcorrencia.setNatureza(jTextFieldNatureza.getText());
        umaOcorrencia.setData(data);
        umaOcorrencia.setHorario(jTextFieldHorario.getText());
        endereco.setLogradouro(jTextFieldLogradouro.getText());
        endereco.setCidade(jTextFieldCidade.getText());
        endereco.setEstado(jTextFieldEstado.getText());
        
        umaOcorrencia.setEndereco(endereco);
        
        umControleOcorrencia.adicionar(umaOcorrencia);
        
        this.carregarListaOcorrencias();
        limparCampos();
    }//GEN-LAST:event_jButtonSalvarActionPerformed

    private void jTextFieldNaturezaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldNaturezaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldNaturezaActionPerformed

    private void jTableOcorrenciasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableOcorrenciasMouseClicked
        if (jTableOcorrencias.isEnabled()) {
            DefaultTableModel model = (DefaultTableModel) jTableOcorrencias.getModel();
            String umGrupo = (String) model.getValueAt(jTableOcorrencias.getSelectedRow(), 0);
            this.pesquisarOcorrencia(umGrupo);
        }
    }//GEN-LAST:event_jTableOcorrenciasMouseClicked

    private void jButtonSalvarPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarPActionPerformed
        Date dataP;
        if (jTextFieldDataP.getText().length() == 0) {
            dataP = null;
        } else {
            try {
                dataP = dateFormat.parse(jTextFieldDataP.getText());
            } catch (ParseException ex) {
                exibirInformacao("Falha ao gravar a data de nascimento: " + ex.toString());
                return;
            }
        }
        String umHorarioP = this.jTextFieldHorarioP.getText();
        umaPrevencao = new Prevencao(dataP, jTextFieldHorarioP.getText());
        
        Endereco endereco = new Endereco();
        
        endereco.setLogradouro(jTextFieldLogradouroP.getText());
        endereco.setCidade(jTextFieldCidadeP.getText());
        endereco.setEstado(jTextFieldEstadoP.getText());
        
        umaPrevencao.setEndereco(endereco);
        
        umControlePrevencao.adicionarPrevencao(umaPrevencao);
        
        this.carregarListaPrevencao();
        limparCampos();
        
    }//GEN-LAST:event_jButtonSalvarPActionPerformed

    private void jTablePrevencaoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTablePrevencaoMouseClicked
        if (jTablePrevencao.isEnabled()) {
            DefaultTableModel model = (DefaultTableModel) jTablePrevencao.getModel();
            Date umaData = (Date) model.getValueAt(jTablePrevencao.getSelectedRow(), 0);
            this.pesquisarPrevencao(umaData);
        }
    }//GEN-LAST:event_jTablePrevencaoMouseClicked

    private void jButtonRemoverPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRemoverPActionPerformed
        this.umControlePrevencao.removerPrevencao(umaPrevencao);        
        this.carregarListaPrevencao();
    }//GEN-LAST:event_jButtonRemoverPActionPerformed

    
    private void pesquisarOcorrencia(String grupo) {
        Ocorrencia ocorrenciaPesquisada = umControleOcorrencia.pesquisar(grupo);

        if (ocorrenciaPesquisada == null) {
            exibirInformacao("Ocorrencia não encontrada.");
        } else {
            this.umaOcorrencia = ocorrenciaPesquisada;           
        }
    }
    
    private void pesquisarPrevencao(Date data) {
        Prevencao prevencaoPesquisada = umControlePrevencao.pesquisarPrevencao(data);

        if (prevencaoPesquisada == null) {
            exibirInformacao("Prevencao não encontrada.");
        } else {
            this.umaPrevencao = prevencaoPesquisada;           
        }
    }
    public static void main(String args[]) {
       
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaBombeiro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaBombeiro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaBombeiro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaBombeiro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaBombeiro().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonRemover;
    private javax.swing.JButton jButtonRemoverP;
    private javax.swing.JButton jButtonSalvar;
    private javax.swing.JButton jButtonSalvarP;
    private javax.swing.JLabel jLabelCidade;
    private javax.swing.JLabel jLabelCidadeP;
    private javax.swing.JLabel jLabelData;
    private javax.swing.JLabel jLabelDataP;
    private javax.swing.JLabel jLabelEstado;
    private javax.swing.JLabel jLabelEstadoP;
    private javax.swing.JLabel jLabelGrupo;
    private javax.swing.JLabel jLabelHorario;
    private javax.swing.JLabel jLabelHorarioP;
    private javax.swing.JLabel jLabelLogradouro;
    private javax.swing.JLabel jLabelLogradouroP;
    private javax.swing.JLabel jLabelNatureza;
    private javax.swing.JLabel jLabelSubgrupo;
    private javax.swing.JPanel jPanelCadastro;
    private javax.swing.JPanel jPanelOcorrências;
    private javax.swing.JPanel jPanelPrevenção;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTableOcorrencias;
    private javax.swing.JTable jTablePrevencao;
    private javax.swing.JTextField jTextFieldCidade;
    private javax.swing.JTextField jTextFieldCidadeP;
    private javax.swing.JTextField jTextFieldData;
    private javax.swing.JTextField jTextFieldDataP;
    private javax.swing.JTextField jTextFieldEstado;
    private javax.swing.JTextField jTextFieldEstadoP;
    private javax.swing.JTextField jTextFieldGrupo;
    private javax.swing.JTextField jTextFieldHorario;
    private javax.swing.JTextField jTextFieldHorarioP;
    private javax.swing.JTextField jTextFieldLogradouro;
    private javax.swing.JTextField jTextFieldLogradouroP;
    private javax.swing.JTextField jTextFieldNatureza;
    private javax.swing.JTextField jTextFieldSubgrupo;
    // End of variables declaration//GEN-END:variables
}
