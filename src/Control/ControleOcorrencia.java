package Control;

import Model.Ocorrencia;
import java.util.ArrayList;

public class ControleOcorrencia {   
    
    private ArrayList<Ocorrencia> listaOcorrencias;
    
    public ControleOcorrencia(){
        this.listaOcorrencias = new ArrayList<Ocorrencia>();
    }

    public ArrayList<Ocorrencia> getListaOcorrencias() {
        return listaOcorrencias;
    }
    
    public void adicionar (Ocorrencia umaOcorrencia){
        listaOcorrencias.add(umaOcorrencia);
    }
    
    public void remover (Ocorrencia umaOcorrencia){
        listaOcorrencias.remove(umaOcorrencia);
    }
        
    public Ocorrencia pesquisar (String grupo){
        for(Ocorrencia o: listaOcorrencias){
            if(o.getGrupo().equalsIgnoreCase(grupo)) return o;
            }
        return null;
    }
    
    
}
