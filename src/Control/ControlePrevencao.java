package Control;

import Model.Prevencao;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

public class ControlePrevencao {
    private ArrayList<Prevencao> listaPrevencao;


    public ControlePrevencao(){
        this.listaPrevencao = new ArrayList<Prevencao>();
    }

    public ArrayList<Prevencao> getListaPrevencao() {
        return listaPrevencao;
    }
    
    public void adicionarPrevencao (Prevencao umaPrevencao){
        listaPrevencao.add(umaPrevencao);
    }
    
    public void removerPrevencao (Prevencao umaPrevencao){
        listaPrevencao.remove(umaPrevencao);
    }
    
    public Prevencao pesquisarPrevencao(Date data) {
        for (Prevencao p: listaPrevencao) {
            if (p.getDataP().equals(data)) return p;
        }
        return null;
    }
    
}