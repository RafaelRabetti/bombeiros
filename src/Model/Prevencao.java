package Model;

import java.util.Date;

public class Prevencao {
    private Date dataP;
    private String horarioP;
    public Endereco endereco;
    
    public Prevencao(Date data, String horario) {
        this.dataP = data;
        this.horarioP = horario;
    }
    
    public Endereco getEndereco() {
        return endereco;
    }

    public void setData(Date data) {
        this.dataP = data;
    }
    
    public void setHorário(String horário) {
        this.horarioP = horário;
    }

    public Date getDataP() {
        return dataP;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public String getHorarioP() {
        return horarioP;
    }
    
}
