package Model;

import java.util.Date;

public class Ocorrencia {
    
    private Date data;
    private String horario;
    private String grupo;
    private String subgrupo;
    private String natureza;
    public Endereco endereco; 
    
    public Ocorrencia(String grupo){
        this.grupo = grupo;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public void setSubgrupo(String subgrupo) {
        this.subgrupo = subgrupo;
    }

    public void setNatureza(String natureza) {
        this.natureza = natureza;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }
    
    public Date getData() {
        return data;
    }

    public String getHorario() {
        return horario;
    }

    public String getGrupo() {
        return grupo;
    }

    public String getSubgrupo() {
        return subgrupo;
    }

    public String getNatureza() {
        return natureza;
    }

    public Endereco getEndereco() {
        return endereco;
    }
    
    
}
